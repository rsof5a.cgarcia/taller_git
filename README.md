## README - Colaborativo

### Conceptos repaso

### Comandos repaso

### Ejercicios propuestos

1. Forkear el proyecto en repositorios propios.
2. Clonar vuestros repositorios.
3. Crear una rama personal - taller_$IDUS.
    - IDUS son las tres primeras letras de vuestro nombre y apellidos. Ej: javjarfer
4. Añadiros en vuestra rama como participantes al taller y hacer un PR.
    - Commandos que os gustaria repasar.
    - Conceptos que queráis repasar durante el taller.
5. Ejercicio de grupo para repasar los cambios.

### Enlaces
https://gitlab.com/carlosgm46/taller_git_cgm.git 
